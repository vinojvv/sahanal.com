var gulp            = require('gulp'),
    concat          = require('gulp-concat'),
    uglify          = require('gulp-uglify'),
    sourcemaps      = require('gulp-sourcemaps'),
    ngAnnotate      = require('gulp-ng-annotate'),
    autoprefixer    = require('gulp-autoprefixer'),
    connect         = require('gulp-connect');

gulp.task('js', function () {
    gulp.src(['scripts/**/main.js', 'scripts/**/*.js'])
        .pipe(sourcemaps.init())
        .pipe(concat('app.js'))
//        .pipe(ngAnnotate())
//        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('build/scripts'))
        .pipe(connect.reload());
});

gulp.task('libjs', function () {
    gulp.src(['libs/**/*.js'])
        .pipe(sourcemaps.init())
        .pipe(concat('libs.js'))
//        .pipe(ngAnnotate())
//        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('build/external'))
        .pipe(connect.reload());
});

gulp.task('css', function () {
    gulp.src(['stylesheets/**/*.css'])
        .pipe(sourcemaps.init())
        .pipe(concat('style.css'))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: true
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('build/stylesheets'))
        .pipe(connect.reload());
});

gulp.task('assets', function () {
    gulp.src(['images/**/*'])
        .pipe(gulp.dest('build/images'));
    
    gulp.src(['fonts/**/*'])
        .pipe(gulp.dest('build/fonts'));
    
    gulp.src(['index.html'])
        .pipe(gulp.dest('build'));
    
    gulp.src(['views/**/*'])
        .pipe(gulp.dest('build/views'));
});

gulp.task('server', function () {
    connect.server({
        port: 8090,
        livereload: { port: 8050 }
    });
});

gulp.task('watch', ['js', 'libjs', 'css', 'assets'], function () {
    gulp.watch('scripts/**/*.js', ['js']);
    gulp.watch('libs/**/*.js', ['libjs']);
    gulp.watch('stylesheets/**/*.css', ['css']);
});

gulp.task('default', ['js', 'libjs', 'css', 'assets', 'server', 'watch']);