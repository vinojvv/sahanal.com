 var topRange      = 700,  // measure from the top of the viewport to X pixels down
     edgeMargin    = 20,   // margin above the top or margin from the end of the page
     animationTime = 1200, // time in milliseconds
     contentTop = [];



$(document).ready(function() {
	$('.mt__secondary-block').waypoint(function(direction) {
		if (direction === 'down') {				  	
			$(".mt__header-block").addClass('show');
		}
	}, { offset: '72px' });
	
	$('.mt__secondary-block').waypoint(function(direction) {
		if (direction === 'up') {				  	
			$(".mt__header-block").removeClass('show');
		}
	}, { offset: '72px' });
	
	
	
	
	 // Stop animated scroll if the user does something
 $('html,body').bind('scroll mousedown DOMMouseScroll mousewheel keyup', function(e){
 if ( e.which > 0 || e.type == 'mousedown' || e.type == 'mousewheel' ){
  $('html,body').stop();
 }
})

 // Set up content an array of locations
 $('.mt__header__nav').find('a').each(function(){
  contentTop.push( $( $(this).attr('href') ).offset().top );
 })

 // Animate menu scroll to content
  $('.mt__header__nav').find('a').click(function(){
   var sel = this,
       newTop = Math.min( contentTop[ $('.mt__header__nav a').index( $(this) ) ], $(document).height() - $(window).height() ); // get content top or top position if at the document bottom
   $('html,body').stop().animate({ 'scrollTop' : newTop }, animationTime, function(){
    window.location.hash = $(sel).attr('href');
   });
   return false;
 })
 
 // adjust side menu
 $(window).scroll(function(){
  var winTop = $(window).scrollTop(),
      bodyHt = $(document).height(),
      vpHt = $(window).height() + edgeMargin;  // viewport height + margin
  $.each( contentTop, function(i,loc){
   if ( ( loc > winTop - edgeMargin && ( loc < winTop + topRange || ( winTop + vpHt ) >= bodyHt ) ) ){
    $('.mt__header__nav a')
     .removeClass('active')
     .eq(i).addClass('active');
   }
  })
 })
 
 
 
 
});